module codeberg.org/BubbyRoosh/dlyt

go 1.16

require (
	github.com/charmbracelet/bubbles v0.8.0
	github.com/charmbracelet/bubbletea v0.14.1
	github.com/kkdai/youtube/v2 v2.7.4
)
