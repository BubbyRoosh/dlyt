# dlyt

Easily download youtube videos with video and audio using bubbletea and goroutines :D.

## Install
```
go get codeberg.org/BubbyRoosh/dlyt
```

## Usage
```
dlyt video-link ...
```
If no links are passed, it will default :)

### Binds
```
ctrl+c or 'q': quit the program early.
```
